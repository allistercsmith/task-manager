# Task Manager

Task Manager is a Node-based, RESTful API for managing tasks for users.

## How to run

Ensure that you have Docker Compose installed, `cd` into the project directory in your terminal, and run `docker-compose up`.

## How to use

Once you have the API running, you can access the following endpoints through CURL, Postman etc. on `http://localhost:3000/api`:

### Create user (`POST http://localhost:3000/api/users`)

```sh
curl -i -H "Content-Type: application/json" -X POST -d '{"username": "jsmith", "first_name": "John", "last_name": "Smith"}' http://localhost:3000/api/users
```

### Update user (`PUT http://localhost:3000/api/users/{user_id}`)

```sh
curl -i -H "Content-Type: application/json" -X PUT -d '{"first_name": "John", "last_name": "Doe"}' http://localhost:3000/api/users/{user_id}
```

### List all users (`GET http://localhost:3000/api/users`)

```sh
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:3000/api/users
```

### Get user info (`GET http://localhost:3000/api/users/{user_id}`)

```sh
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:3000/api/users/{user_id}
```

### Delete user (`DELETE http://localhost:3000/api/users/{user_id}`)

```sh
curl -i -H "Content-Type: application/json" -X DELETE http://localhost:3000/api/users/{user_id}
```

### Create task (`POST http://localhost:3000/api/users/{user_id}/tasks`)

```sh
curl -i -H "Content-Type: application/json" -X POST -d '{"name": "My task", "description": "Description of task", "date_time": "2016-05-25 14:25:00"}' http://localhost:3000/api/users/{user_id}/tasks
```

### Update task (`PUT http://localhost:3000/api/users/{user_id}/tasks/{task_id}`)

```sh
curl -i -H "Content-Type: application/json" -X PUT -d '{"name": "My updated task"}' http://localhost:3000/api/users/{user_id}/tasks/{task_id}
```

### Delete task (`DELETE http://localhost:3000/api/users/{user_id}/tasks/{task_id}`)

```sh
curl -i -H "Content-Type: application/json" -X DELETE http://localhost:3000/api/users/{user_id}/tasks/{task_id}
```

### Get task info (`GET http://localhost:3000/api/users/{user_id}/tasks/{task_id}`)

```sh
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:3000/api/users/{user_id}/tasks/{task_id}
```

### List all tasks for user (`GET http://localhost:3000/api/users/{user_id}/tasks`)

```sh
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:3000/api/users/{user_id}/tasks
```

## Automated jobs

### Overdue tasks

The API includes an automated job to process any overdue tasks. An overdue task is one having a `date_time` value in the past, which hasn't been marked as done. The job is configured to run every minute.

## Testing

Both the task and user service layers are fully unit tested. To run the test suite, `cd` into the project directory in your terminal, and run `docker-compose run api npm test`.

## Todo

There's still a lot that can be done with this project, specifically:

* Better validation in the service layer
* Introduce the concept of shared tasks
* Authentication
* Integration tests for the HTTP and model layers
