"use strict";

exports.up = (knex) => {
    return knex.schema.createTable("tasks", (table) => {
        table.increments("id").primary();
        table.integer("user_id").unsigned().references("id").inTable("users").notNullable().onDelete("CASCADE");
        table.string("name");
        table.string("description");
        table.dateTime("date_time");
    });
};

exports.down = (knex) => {
    return knex.schema.dropTable("tasks");
};
