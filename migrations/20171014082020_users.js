"use strict";

exports.up = (knex) => {
    return knex.schema.createTable("users", (table) => {
        table.increments("id").primary();
        table.string("username");
        table.string("first_name");
        table.string("last_name");
        table.unique("username");
    });
};

exports.down = (knex) => {
    return knex.schema.dropTable("users");
};
