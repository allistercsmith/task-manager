"use strict";

exports.up = function(knex) {
    return knex.schema
        .createTable("task_statuses", (table) => {
            table.increments("id").primary();
            table.string("key");
            table.string("description");
            table.unique("key");
        })
        .then(() => {
            return knex("task_statuses").insert([{
                id: 1,
                key: "pending",
                description: "Task still pending",
            }, {
                id: 2,
                key: "done",
                description: "Task complete",
            }]);
        })
        .then(() => {
            return knex.schema.table("tasks", (table) => {
                table.integer("task_status_id").unsigned().references("id").inTable("task_statuses").notNullable().onDelete("CASCADE").defaultTo(1);
            });
        });
};

exports.down = function(knex) {
    return knex.schema
        .table("tasks", (table) => {
            table.dropForeign("task_status_id");
            table.dropColumn("task_status_id");
        })
        .then(() => {
            return knex.schema.dropTable("task_statuses");
        });
};
