/**
 * User use case tests.
 *
 * @module test/users
 */

"use strict";

const assert = require("chai").assert;
const chance = require("chance").Chance();
const UserErrors = require("../../lib/users/errors");
const UserModel = require("../../lib/users/user");
const UserService = require("../../lib/users/service");
const _ = require("lodash");

describe("User use cases", function() {
    describe("#createUser()", function() {
        _.forEach([
            null,
            undefined,
            "abc",
            123,
        ], (data, key) => {
            it(`should fail with invalid data param (case ${key})`, function(done) {
                UserService
                    .createUser(data)
                    .then(() => assert.fail())
                    .catch(() => done());
            });
        });

        it("should fail if the username exists", function() {
            const data = {
                "first_name": chance.first(),
                "last_name": chance.last(),
                "username": chance.word(),
            };

            const user = Object.assign(data, {
                id: chance.integer({
                    min: 0,
                    max: 1000,
                }),
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findByUsername")
                .resolves(new UserModel(user));

            return UserService
                .createUser(data)
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, UserErrors.UsernameExistsError);
                });
        });

        it("should create a new user", function() {
            const data = {
                "first_name": chance.first(),
                "last_name": chance.last(),
                "username": chance.word(),
            };

            const newUser = Object.assign(data, {
                id: chance.integer({
                    min: 0,
                    max: 1000,
                }),
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findByUsername")
                .resolves(null);

            this
                .sandbox
                .mock(UserModel.prototype)
                .expects("save")
                .resolves(new UserModel(newUser));

            return UserService
                .createUser(data)
                .then((user) => {
                    assert.deepEqual(user, newUser);
                });
        });
    });

    describe("#updateUser()", function() {
        _.forEach([{
            id: null,
            data: {},
        }, {
            id: 1,
            data: "abc",
        }, {
            id: 1,
            data: undefined,
        }], (params, key) => {
            it(`should fail with invalid parameters (case ${key})`, function(done) {
                UserService
                    .updateUser(params.id, params.data)
                    .then(() => assert.fail())
                    .catch(() => done());
            });
        });

        it("should fail if the user doesn't exist", function() {
            const id = chance.integer({
                min: 0,
                max: 1000,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(null);

            this
                .sandbox
                .mock(UserModel)
                .expects("findByUsername")
                .resolves(null);

            return UserService
                .updateUser(id, {})
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, UserErrors.UserNotFoundError);
                });
        });

        it("should fail if the username already exists", function() {
            const id = chance.integer({
                min: 0,
                max: 1000,
            });

            const data = {
                "first_name": chance.first(),
                "last_name": chance.last(),
                "username": chance.word(),
            };

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel(Object.assign(data, {
                    id,
                })));

            this
                .sandbox
                .mock(UserModel)
                .expects("findByUsername")
                .resolves(new UserModel({
                    "id": chance.integer({
                        min: 0,
                        max: 1000,
                    }),
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            return UserService
                .updateUser(id, data)
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, UserErrors.UsernameExistsError);
                });
        });

        it("should update an existing user", function() {
            const id = chance.integer({
                min: 0,
                max: 1000,
            });

            const data = {
                "first_name": chance.first(),
            };

            const oldUser = {
                id,
                "first_name": chance.first(),
                "last_name": chance.last(),
                "username": chance.word(),
            };

            const newUser = Object.assign(oldUser, data);

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel(oldUser));

            this
                .sandbox
                .mock(UserModel.prototype)
                .expects("save")
                .resolves(new UserModel(newUser));

            this
                .sandbox
                .mock(UserModel)
                .expects("findByUsername")
                .resolves(null);

            return UserService
                .updateUser(id, data)
                .then((user) => {
                    assert.deepEqual(user, newUser);
                });
        });
    });

    describe("#deleteUser()", function() {
        _.forEach([
            null,
            undefined,
            "abc",
            "",
        ], (id, key) => {
            it(`should fail with invalid ID param (case ${key})`, function(done) {
                UserService
                    .deleteUser(id)
                    .then(() => assert.fail())
                    .catch(() => done());
            });
        });

        it("should fail if the user doesn't exist", function() {
            const id = chance.integer({
                min: 0,
                max: 1000,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(null);

            return UserService
                .deleteUser(id)
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, UserErrors.UserNotFoundError);
                });
        });

        it("should delete a single user by ID", function() {
            const expected = {
                "id": chance.integer({
                    min: 0,
                    max: 1000,
                }),
                "first_name": chance.first(),
                "last_name": chance.last(),
                "username": chance.word(),
            };

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel(expected));

            this
                .sandbox
                .mock(UserModel.prototype)
                .expects("destroy")
                .resolves(null);

            return UserService
                .deleteUser(expected["id"]);
        });
    });

    describe("#listAllUsers()", function() {
        it("should find all users", function() {
            const expected = [];
            const users = UserModel.collection();
            const count = chance.integer({
                min: 5,
                max: 10,
            });

            for (let i = 0; i < count; i++) {
                const user = {
                    "id": chance.integer({
                        min: 0,
                        max: 1000,
                    }),
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                };

                expected.push(user);
                users.push(user);
            }

            this
                .sandbox
                .mock(UserModel)
                .expects("findAll")
                .returns(users);

            return UserService
                .listAllUsers()
                .then((users) => {
                    assert.deepEqual(users, expected);
                });
        });
    });

    describe("#getUserInfo()", function() {
        _.forEach([
            null,
            undefined,
            "abc",
            "",
        ], (id, key) => {
            it(`should fail with invalid ID param (case ${key})`, function(done) {
                UserService
                    .getUserInfo(id)
                    .then(() => assert.fail())
                    .catch(() => done());
            });
        });

        it("should fail if the user doesn't exist", function() {
            const id = chance.integer({
                min: 0,
                max: 1000,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(null);

            return UserService
                .getUserInfo(id)
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, UserErrors.UserNotFoundError);
                });
        });

        it("should find a single user by ID", function() {
            const expected = {
                "id": chance.integer({
                    min: 0,
                    max: 1000,
                }),
                "first_name": chance.first(),
                "last_name": chance.last(),
                "username": chance.word(),
            };

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel(expected));

            return UserService
                .getUserInfo(expected["id"])
                .then((user) => {
                    assert.deepEqual(user, expected);
                });
        });
    });
});
