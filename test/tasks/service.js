/**
 * Task use case tests.
 *
 * @module test/tasks
 */

"use strict";

const assert = require("chai").assert;
const chance = require("chance").Chance();
const moment = require("moment");
const TaskErrors = require("../../lib/tasks/errors");
const TaskModel = require("../../lib/tasks/task");
const TaskService = require("../../lib/tasks/service");
const UserErrors = require("../../lib/users/errors");
const UserModel = require("../../lib/users/user");
const _ = require("lodash");

describe("Task use cases", function() {
    describe("#listAllTasks()", function() {
        _.forEach([
            null,
            undefined,
            "abc",
            "",
        ], (id, key) => {
            it(`should fail with invalid userId param (case ${key})`, function(done) {
                TaskService
                    .listAllTasks(id)
                    .then(() => assert.fail())
                    .catch(() => done());
            });
        });

        it("should fail if the user doesn't exist", function() {
            const userId = chance.integer({
                min: 0,
                max: 1000,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(null);

            return TaskService
                .listAllTasks(userId)
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, UserErrors.UserNotFoundError);
                });
        });

        it("should find all tasks for the given user", function() {
            const expected = [];
            const tasks = TaskModel.collection();
            const userId = chance.integer({
                min: 0,
                max: 1000,
            });
            const count = chance.integer({
                min: 5,
                max: 10,
            });

            for (let i = 0; i < count; i++) {
                const task = {
                    "id": chance.integer({
                        min: 0,
                        max: 1000,
                    }),
                    "user_id": userId,
                    "name": chance.word(),
                    "description": chance.sentence(),
                    "date_time": chance.date({
                        string: true,
                    }),
                    "task_status_id": chance.integer({
                        min: 1,
                        max: 1,
                    }),
                };

                expected.push(task);
                tasks.push(task);
            }

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .returns(new UserModel({
                    "id": userId,
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            this
                .sandbox
                .mock(TaskModel)
                .expects("findByUserId")
                .returns(tasks);

            return TaskService
                .listAllTasks(userId)
                .then((tasks) => {
                    assert.deepEqual(tasks, expected);
                });
        });
    });

    describe("#getTaskInfo()", function() {
        _.forEach([{
            taskId: null,
            userId: 2,
        }, {
            taskId: undefined,
            userId: null,
        }, {
            taskId: 1,
            userId: null,
        }, {
            taskId: "abc",
            userId: 2,
        }], (params, key) => {
            it(`should fail with invalid parameters (case ${key})`, function(done) {
                TaskService
                    .getTaskInfo(params.taskId, params.userId)
                    .then(() => assert.fail())
                    .catch(() => done());
            });
        });

        it("should fail if the user doesn't exist", function() {
            const taskId = chance.integer({
                min: 0,
                max: 1000,
            });
            const userId = chance.integer({
                min: 0,
                max: 1000,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(null);

            return TaskService
                .getTaskInfo(taskId, userId)
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, UserErrors.UserNotFoundError);
                });
        });

        it("should fail if the task doesn't exist", function() {
            const taskId = chance.integer({
                min: 0,
                max: 1000,
            });
            const userId = chance.integer({
                min: 0,
                max: 1000,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel({
                    "id": userId,
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            this
                .sandbox
                .mock(TaskModel)
                .expects("findById")
                .resolves(null);

            return TaskService
                .getTaskInfo(taskId, userId)
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, TaskErrors.TaskNotFoundError);
                });
        });

        it("should fail if the task doesn't belong to the user", function() {
            const taskId = chance.integer({
                min: 0,
                max: 100,
            });
            const userId = chance.integer({
                min: 0,
                max: 100,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel({
                    "id": userId,
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            this
                .sandbox
                .mock(TaskModel)
                .expects("findById")
                .resolves(new TaskModel({
                    "id": taskId,
                    "userId": chance.integer({
                        min: 101,
                        max: 200,
                    }),
                    "name": chance.word(),
                    "description": chance.sentence(),
                    "date_time": chance.date({
                        string: true,
                    }),
                    "task_status_id": chance.integer({
                        min: 1,
                        max: 1,
                    }),
                }));

            return TaskService
                .getTaskInfo(taskId, userId)
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, TaskErrors.InvalidUserError);
                });
        });

        it("should retrieve a given task for the user", function() {
            const taskId = chance.integer({
                min: 0,
                max: 100,
            });
            const userId = chance.integer({
                min: 0,
                max: 100,
            });
            const expected = {
                "id": taskId,
                "user_id": userId,
                "name": chance.word(),
                "description": chance.sentence(),
                "date_time": chance.date({
                    string: true,
                }),
                "task_status_id": chance.integer({
                    min: 1,
                    max: 1,
                }),
            };

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel({
                    "id": userId,
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            this
                .sandbox
                .mock(TaskModel)
                .expects("findById")
                .resolves(new TaskModel(expected));

            return TaskService
                .getTaskInfo(taskId, userId)
                .then((task) => {
                    assert.deepEqual(task, expected);
                });
        });
    });

    describe("#deleteTask()", function() {
        _.forEach([{
            taskId: null,
            userId: 2,
        }, {
            taskId: undefined,
            userId: null,
        }, {
            taskId: 1,
            userId: null,
        }, {
            taskId: "abc",
            userId: 2,
        }], (params, key) => {
            it(`should fail with invalid parameters (case ${key})`, function(done) {
                TaskService
                    .deleteTask(params.taskId, params.userId)
                    .then(() => assert.fail())
                    .catch(() => done());
            });
        });

        it("should fail if the user doesn't exist", function() {
            const taskId = chance.integer({
                min: 0,
                max: 1000,
            });
            const userId = chance.integer({
                min: 0,
                max: 1000,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(null);

            return TaskService
                .deleteTask(taskId, userId)
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, UserErrors.UserNotFoundError);
                });
        });

        it("should fail if the task doesn't exist", function() {
            const taskId = chance.integer({
                min: 0,
                max: 1000,
            });
            const userId = chance.integer({
                min: 0,
                max: 1000,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel({
                    "id": userId,
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            this
                .sandbox
                .mock(TaskModel)
                .expects("findById")
                .resolves(null);

            return TaskService
                .deleteTask(taskId, userId)
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, TaskErrors.TaskNotFoundError);
                });
        });

        it("should fail if the task doesn't belong to the user", function() {
            const taskId = chance.integer({
                min: 0,
                max: 100,
            });
            const userId = chance.integer({
                min: 0,
                max: 100,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel({
                    "id": userId,
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            this
                .sandbox
                .mock(TaskModel)
                .expects("findById")
                .resolves(new TaskModel({
                    "id": taskId,
                    "userId": chance.integer({
                        min: 101,
                        max: 200,
                    }),
                    "name": chance.word(),
                    "description": chance.sentence(),
                    "date_time": chance.date({
                        string: true,
                    }),
                    "task_status_id": chance.integer({
                        min: 1,
                        max: 1,
                    }),
                }));

            return TaskService
                .deleteTask(taskId, userId)
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, TaskErrors.InvalidUserError);
                });
        });

        it("should delete a given task for the user", function() {
            const taskId = chance.integer({
                min: 0,
                max: 100,
            });
            const userId = chance.integer({
                min: 0,
                max: 100,
            });
            const expected = {
                "id": taskId,
                "user_id": userId,
                "name": chance.word(),
                "description": chance.sentence(),
                "date_time": chance.date({
                    string: true,
                }),
                "task_status_id": chance.integer({
                    min: 1,
                    max: 1,
                }),
            };

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel({
                    "id": userId,
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            this
                .sandbox
                .mock(TaskModel)
                .expects("findById")
                .resolves(new TaskModel(expected));

            this
                .sandbox
                .mock(TaskModel.prototype)
                .expects("destroy")
                .resolves(null);

            return TaskService
                .deleteTask(taskId, userId);
        });
    });

    describe("#updateTask()", function() {
        _.forEach([{
            taskId: null,
            userId: 2,
            data: {},
        }, {
            taskId: undefined,
            userId: null,
            data: {},
        }, {
            taskId: 1,
            userId: null,
            data: {},
        }, {
            taskId: "abc",
            userId: 2,
            data: {},
        }, {
            taskId: 1,
            userId: 2,
            data: "abc",
        }, {
            taskId: 1,
            userId: 2,
            data: null,
        }], (params, key) => {
            it(`should fail with invalid parameters (case ${key})`, function(done) {
                TaskService
                    .updateTask(params.taskId, params.userId, params.data)
                    .then(() => assert.fail())
                    .catch(() => done());
            });
        });

        it("should fail if the user doesn't exist", function() {
            const taskId = chance.integer({
                min: 0,
                max: 1000,
            });
            const userId = chance.integer({
                min: 0,
                max: 1000,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(null);

            return TaskService
                .updateTask(taskId, userId, {})
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, UserErrors.UserNotFoundError);
                });
        });

        it("should fail if the task doesn't exist", function() {
            const taskId = chance.integer({
                min: 0,
                max: 1000,
            });
            const userId = chance.integer({
                min: 0,
                max: 1000,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel({
                    "id": userId,
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            this
                .sandbox
                .mock(TaskModel)
                .expects("findById")
                .resolves(null);

            return TaskService
                .updateTask(taskId, userId, {})
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, TaskErrors.TaskNotFoundError);
                });
        });

        it("should fail if the task doesn't belong to the user", function() {
            const taskId = chance.integer({
                min: 0,
                max: 100,
            });
            const userId = chance.integer({
                min: 0,
                max: 100,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel({
                    "id": userId,
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            this
                .sandbox
                .mock(TaskModel)
                .expects("findById")
                .resolves(new TaskModel({
                    "id": taskId,
                    "userId": chance.integer({
                        min: 101,
                        max: 200,
                    }),
                    "name": chance.word(),
                    "description": chance.sentence(),
                    "date_time": chance.date({
                        string: true,
                    }),
                    "task_status_id": chance.integer({
                        min: 1,
                        max: 1,
                    }),
                }));

            return TaskService
                .updateTask(taskId, userId, {})
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, TaskErrors.InvalidUserError);
                });
        });

        it("should update a given task for the user", function() {
            const taskId = chance.integer({
                min: 0,
                max: 100,
            });
            const userId = chance.integer({
                min: 0,
                max: 100,
            });

            const data = {
                "description": chance.sentence(),
            };

            const oldTask = {
                "id": taskId,
                "user_id": userId,
                "name": chance.word(),
                "description": chance.sentence(),
                "date_time": chance.date({
                    string: true,
                }),
                "task_status_id": chance.integer({
                    min: 1,
                    max: 1,
                }),
            };

            const newTask = Object.assign(oldTask, data);

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel({
                    "id": userId,
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            this
                .sandbox
                .mock(TaskModel)
                .expects("findById")
                .resolves(new TaskModel(oldTask));

            this
                .sandbox
                .mock(TaskModel.prototype)
                .expects("save")
                .resolves(null);

            return TaskService
                .updateTask(taskId, userId, data)
                .then((task) => {
                    assert.deepEqual(task, newTask);
                });
        });
    });

    describe("#createTask()", function() {
        _.forEach([{
            userId: null,
            data: {},
        }, {
            userId: 1,
            data: null,
        }, {
            userId: undefined,
            data: {},
        }, {
            userId: 2,
            data: undefined,
        }, {
            userId: 2,
            data: "abc",
        }], (params, key) => {
            it(`should fail with invalid parameters (case ${key})`, function(done) {
                TaskService
                    .createTask(params.userId, params.data)
                    .then(() => assert.fail())
                    .catch(() => done());
            });
        });

        it("should fail if the user doesn't exist", function() {
            const userId = chance.integer({
                min: 0,
                max: 1000,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(null);

            return TaskService
                .createTask(userId, {})
                .then(() => assert.fail())
                .catch((err) => {
                    assert.instanceOf(err, UserErrors.UserNotFoundError);
                });
        });

        it("should update a given task for the user", function() {
            const taskId = chance.integer({
                min: 0,
                max: 100,
            });
            const userId = chance.integer({
                min: 0,
                max: 100,
            });

            const data = {
                "user_id": userId,
                "name": chance.word(),
                "description": chance.sentence(),
                "date_time": chance.date({
                    string: true,
                }),
                "task_status_id": chance.integer({
                    min: 1,
                    max: 1,
                }),
            };

            const expected = Object.assign(data, {
                "id": taskId,
            });

            this
                .sandbox
                .mock(UserModel)
                .expects("findById")
                .resolves(new UserModel({
                    "id": userId,
                    "first_name": chance.first(),
                    "last_name": chance.last(),
                    "username": chance.word(),
                }));

            this
                .sandbox
                .mock(TaskModel.prototype)
                .expects("save")
                .resolves(new TaskModel(expected));

            return TaskService
                .createTask(userId, data)
                .then((task) => {
                    assert.deepEqual(task, expected);
                });
        });
    });

    describe("#updateOverdueTasks()", function() {
        it("should update overdue tasks", function() {
            const lastYear = moment().subtract(1, "y").format("YYYY");
            const tasks = TaskModel.collection();

            const overdueTask = new TaskModel({
                "id": chance.integer({
                    min: 1,
                    max: 100,
                }),
                "user_id": chance.integer({
                    min: 1,
                    max: 100,
                }),
                "name": chance.word(),
                "description": chance.sentence(),
                "date_time": chance.date({
                    string: true,
                    year: lastYear,
                }),
                "task_status_id": chance.integer({
                    min: 1,
                    max: 1,
                }),
            });

            tasks.add(overdueTask);

            this
                .sandbox
                .mock(TaskModel)
                .expects("findOverdue")
                .resolves(tasks);

            this
                .sandbox
                .mock(overdueTask)
                .expects("save")
                .once();

            return TaskService
                .updateOverdueTasks();
        });
    });
});
