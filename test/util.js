/**
 * General test utilities.
 *
 * @module test/util
 */

"use strict";

const sinon = require("sinon");

before(function() {
    this.sandbox = sinon.sandbox.create();
});

afterEach(function() {
    this.sandbox.restore();
});
