FROM node:8-alpine

ARG environment=develop

RUN apk update && \
    apk add ca-certificates wget git && \
    update-ca-certificates

WORKDIR /tmp

RUN wget https://github.com/jwilder/dockerize/releases/download/v0.1.0/dockerize-linux-amd64-v0.1.0.tar.gz
RUN tar -C /usr/local/bin -xzvf dockerize-linux-amd64-v0.1.0.tar.gz

COPY . /app
WORKDIR /app

ENV NODE_ENV $environment
RUN npm install

VOLUME /app

CMD dockerize -timeout 60s -wait tcp://$DB_HOST:3306 && \
    npm run migrate && \
    npm start
