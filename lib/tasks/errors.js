/**
 * General task-related errors.
 *
 * @module lib/tasks/errors
 */

"use strict";

class TaskNotFoundError extends Error {
    constructor(...args) {
        super(...args);

        Error.captureStackTrace(this, TaskNotFoundError);
    }
}

class InvalidUserError extends Error {
    constructor(...args) {
        super(...args);

        Error.captureStackTrace(this, InvalidUserError);
    }
}

module.exports = {
    TaskNotFoundError,
    InvalidUserError,
};
