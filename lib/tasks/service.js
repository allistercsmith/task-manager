/**
 * Task use cases.
 *
 * @module lib/tasks/service
 */

"use strict";

const TaskErrors = require("./errors");
const Task = require("./task");
const UserErrors = require("../users/errors");
const User = require("../users/user");
const _ = require("lodash");

/**
 * A list of task statuses and their associated database keys.
 *
 * @todo Make this nicer by reading from the DB instead.
 */
const statuses = {
    "pending": 1,
    "done": 2,
};

module.exports = {
    /**
     * Creates a new task for a user.
     *
     * Will throw a UserNotFoundError if the user cannot be found.
     *
     * @param {number} userId The user's ID
     * @param {object} data The data to create a new task from
     * @return {object}
     */
    createTask: async function(userId, data) {
        if (!_.isNumber(userId)) {
            throw new Error("`userId` needs to be a number");
        }

        if (!_.isPlainObject(data)) {
            throw new Error("`data` needs to be a plain object");
        }

        const user = await User.findById(userId);

        if (_.isNil(user)) {
            throw new UserErrors.UserNotFoundError();
        }

        let task = new Task(Object.assign(data, {
            "user_id": userId,
        }));

        task = await task.save();

        return task.toJSON();
    },

    /**
     * Updates a task for a user.
     *
     * Will throw the following errors:
     *
     * 1) UserNotFoundError if the user doesn't exist
     * 2) TaskNotFoundError if the task doesn't exist
     * 3) InvalidUserError if the user doesn't own the task
     *
     * @param {number} taskId The task ID
     * @param {number} userId The user's ID
     * @param {object} data The data to update the task with
     * @return {object}
     */
    updateTask: async function(taskId, userId, data) {
        if (!_.isNumber(taskId)) {
            throw new Error("`taskId` needs to be a number");
        }

        if (!_.isNumber(userId)) {
            throw new Error("`userId` needs to be a number");
        }

        if (!_.isPlainObject(data)) {
            throw new Error("`data` needs to be a plain object");
        }

        const user = await User.findById(userId);

        if (_.isNil(user)) {
            throw new UserErrors.UserNotFoundError();
        }

        const task = await Task.findById(taskId);

        if (_.isNil(task)) {
            throw new TaskErrors.TaskNotFoundError();
        }

        if (task.get("user_id") !== userId) {
            throw new TaskErrors.InvalidUserError();
        }

        task.set(data);

        await task.save();

        return task.toJSON();
    },

    /**
     * Deletes a task for a user.
     *
     * Will throw the following errors:
     *
     * 1) UserNotFoundError if the user doesn't exist
     * 2) TaskNotFoundError if the task doesn't exist
     * 3) InvalidUserError if the user doesn't own the task
     *
     * @param {number} taskId The task ID
     * @param {number} userId The user's ID
     * @return {object}
     */
    deleteTask: async function(taskId, userId) {
        if (!_.isNumber(taskId)) {
            throw new Error("`taskId` needs to be a number");
        }

        if (!_.isNumber(userId)) {
            throw new Error("`userId` needs to be a number");
        }

        const user = await User.findById(userId);

        if (_.isNil(user)) {
            throw new UserErrors.UserNotFoundError();
        }

        const task = await Task.findById(taskId);

        if (_.isNil(task)) {
            throw new TaskErrors.TaskNotFoundError();
        }

        if (task.get("user_id") !== userId) {
            throw new TaskErrors.InvalidUserError();
        }

        await task.destroy();
    },

    /**
     * Returns a specific task for a user.
     *
     * Will throw the following errors:
     *
     * 1) UserNotFoundError if the user doesn't exist
     * 2) TaskNotFoundError if the task doesn't exist
     * 3) InvalidUserError if the user doesn't own the task
     *
     * @param {number} taskId The task ID
     * @param {number} userId The user's ID
     * @return {object}
     */
    getTaskInfo: async function(taskId, userId) {
        if (!_.isNumber(taskId)) {
            throw new Error("`taskId` needs to be a number");
        }

        if (!_.isNumber(userId)) {
            throw new Error("`userId` needs to be a number");
        }

        const user = await User.findById(userId);

        if (_.isNil(user)) {
            throw new UserErrors.UserNotFoundError();
        }

        const task = await Task.findById(taskId);

        if (_.isNil(task)) {
            throw new TaskErrors.TaskNotFoundError();
        }

        if (task.get("user_id") !== userId) {
            throw new TaskErrors.InvalidUserError();
        }

        return task.toJSON();
    },

    /**
     * Returns a list of all tasks for the given user.
     *
     * Will throw a UserNotFound error if the user doesn't exist.
     *
     * @param {number} userId The user's ID
     * @return {object}
     */
    listAllTasks: async function(userId) {
        if (!_.isNumber(userId)) {
            throw new Error("`userId` needs to be a number");
        }

        const user = await User.findById(userId);

        if (_.isNil(user)) {
            throw new UserErrors.UserNotFoundError();
        }

        const tasks = await Task.findByUserId(userId);

        return tasks.toJSON();
    },

    /**
     * Marks all overdue tasks (where their date_time has passed)
     * as done by updating their status.
     */
    updateOverdueTasks: async function() {
        const overdueTasks = await Task.findOverdue();

        for (let i = 0; i < overdueTasks.length; i++) {
            overdueTasks.at(i).set("task_status_id", statuses["done"]);

            await overdueTasks.at(i).save();
        }
    },
};
