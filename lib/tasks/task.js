/**
 * Task domain model.
 *
 * @module lib/tasks/task
 */

"use strict";

const db = require("../db");
const moment = require("moment");
const _ = require("lodash");

const Task = db.Model.extend({
    tableName: "tasks",
}, {
    /**
     * Finds all tasks for a given user ID.
     *
     * @param {number} userId The user ID to search by
     * @return {Promise}
     */
    findByUserId: async function(userId) {
        if (!_.isFinite(userId)) {
            throw new Error("`userId` needs to be a number");
        }

        return this.where("user_id", userId).fetchAll();
    },

    /**
     * Finds a single task by ID.
     *
     * @param {number} taskId The task ID to search by
     * @return {Promise}
     */
    findById: async function(taskId) {
        if (!_.isFinite(taskId)) {
            throw new Error("`taskId` needs to be a number");
        }

        return this.where("id", taskId).fetch();
    },

    /**
     * Finds all overdue tasks (where date_time has passed,
     * and status is not done).
     *
     * @return {Promise}
     */
    findOverdue: async function() {
        return this
            .query(function(qb) {
                qb
                    .whereNot("task_status_id", function() {
                        this
                            .select("id")
                            .from("task_statuses")
                            .where("key", "done");
                    })
                    .andWhere("date_time", "<", moment().format("YYYY-MM-DD"));
            })
            .fetchAll();
    },
});

module.exports = Task;
