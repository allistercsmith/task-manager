/**
 * Bookshelf database connection.
 *
 * @module lib/db
 */

"use strict";

const Bookshelf = require("bookshelf");
const config = require("../knexfile");
const Knex = require("knex");

const knex = Knex(config);
const bookshelf = Bookshelf(knex);

module.exports = bookshelf;
