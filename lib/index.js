/**
 * Entrypoint for the application.
 *
 * @module lib
 */

"use strict";

const cron = require("cron");
const Http = require("./http");
const logger = require("./logger");
const TaskJobs = require("./jobs/task");

const server = Http(3000, "/api");
const overdueTaskCheck = new cron.CronJob("*/60 * * * * *", async () => {
    try {
        await TaskJobs.processOverdueTasks();
    } catch (err) {
        logger.error(err);
    }
});

overdueTaskCheck.start();
server.start();
