/**
 * User-related HTTP routes.
 *
 * @module lib/http/users
 */

"use strict";

const Joi = require("joi");
const UserErrors = require("../users/errors");
const UserService = require("../users/service");
const _ = require("lodash");

module.exports = {
    /**
     * Returns a single user by ID.
     */
    get: async function(req, res, next) {
        const userId = _.toNumber(req.params.userId);

        if (!_.isFinite(userId)) {
            res.status(400).error("Invalid user ID param");

            return next();
        }

        try {
            const user = await UserService.getUserInfo(userId);

            res.status(200).json(user);
        } catch (err) {
            if (err instanceof UserErrors.UserNotFoundError) {
                res.status(404).error("User not found");
            } else {
                return next(err);
            }
        }
    },

    /**
     * Returns all users.
     */
    getAll: async function(req, res, next) {
        try {
            const users = await UserService.listAllUsers();

            res.status(200).json(users);
        } catch (err) {
            return next(err);
        }
    },

    /**
     * Creates a new user.
     */
    create: async function(req, res, next) {
        const payload = Joi.validate(req.body, Joi.object().keys({
            "username": Joi.string().required(),
            "first_name": Joi.string().required(),
            "last_name": Joi.string().required(),
        }), {
            stripUnknown: {
                objects: true,
            },
            abortEarly: false,
        });

        if (payload.error) {
            res.status(400).error("Request failed validation", payload.error);
        } else {
            try {
                const user = await UserService.createUser(payload.value);

                res.status(201).json(user);
            } catch (err) {
                if (err instanceof UserErrors.UsernameExistsError) {
                    res.status(409).error("Username already exists");
                } else {
                    return next(err);
                }
            }
        }
    },

    /**
     * Updates a single user by ID.
     */
    update: async function(req, res, next) {
        const userId = _.toNumber(req.params.userId);

        if (!_.isFinite(userId)) {
            res.status(400).error("Invalid user ID param");

            return next();
        }

        const payload = Joi.validate(req.body, Joi.object().keys({
            "username": Joi.string(),
            "first_name": Joi.string(),
            "last_name": Joi.string(),
        }), {
            stripUnknown: {
                objects: true,
            },
            abortEarly: false,
        });

        if (payload.error) {
            res.status(400).error("Request failed validation", payload.error);
        } else {
            try {
                const user = await UserService.updateUser(userId, req.body);

                res.status(200).json(user);
            } catch (err) {
                if (err instanceof UserErrors.UserNotFoundError) {
                    res.status(404).error("User not found");
                } else if (err instanceof UserErrors.UsernameExistsError) {
                    res.status(409).error("Username already exists");
                } else {
                    return next(err);
                }
            }
        }
    },

    /**
     * Deletes a single user by ID.
     */
    delete: async function(req, res, next) {
        const userId = _.toNumber(req.params.userId);

        if (!_.isFinite(userId)) {
            res.status(400).error("Invalid user ID param");

            return next();
        }

        try {
            await UserService.deleteUser(userId);

            res.status(204).send();
        } catch (err) {
            if (err instanceof UserErrors.UserNotFoundError) {
                res.status(404).error("User not found");
            } else {
                return next(err);
            }
        }
    },
};
