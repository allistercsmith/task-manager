/**
 * Task Manager HTTP server.
 *
 * @module lib/http
 */

"use strict";

const bodyParser = require("body-parser");
const Express = require("express");
const logger = require("../logger");
const middleware = require("./middleware");
const Routes = require("./routes");

/**
 * Creates a new HTTP server to serve the API.
 *
 * @param {number} port The port to listen on (will default to 3000 if not provided)
 * @param {string} basePath The base path to serve on (will default to "/"" if not provided)
 * @return {object}
 */
module.exports = function(port, basePath) {
    port = port || 3000;
    basePath = basePath || "";

    const app = Express();

    app.use(bodyParser.json());
    app.use(middleware.response);
    app.use(basePath, Routes());
    app.use(middleware.error);

    return {
        /**
         * Starts the HTTP server.
         */
        start: () => {
            app.listen(port, () => {
                logger.info(`Application started on port ${port}`);
            });
        },
    };
};
