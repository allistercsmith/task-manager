/**
 * HTTP router for the API.
 *
 * @module lib/http/routes
 */

"use strict";

const express = require("express");
const Task = require("./tasks");
const User = require("./users");

/**
 * Creates and returns a router with the API endpoints registered.
 *
 * @return {express.Router}
 */
module.exports = function() {
    const router = express.Router();

    router.get("/users", User.getAll);
    router.get("/users/:userId", User.get);
    router.post("/users", User.create);
    router.put("/users/:userId", User.update);
    router.delete("/users/:userId", User.delete);

    router.get("/users/:userId/tasks", Task.getAll);
    router.get("/users/:userId/tasks/:taskId", Task.get);
    router.post("/users/:userId/tasks", Task.create);
    router.put("/users/:userId/tasks/:taskId", Task.update);
    router.delete("/users/:userId/tasks/:taskId", Task.delete);

    return router;
};
