/**
 * Task-related HTTP routes.
 *
 * @module lib/http/tasks
 */


"use strict";

const Joi = require("joi");
const TaskService = require("../tasks/service");
const TaskErrors = require("../tasks/errors");
const UserErrors = require("../users/errors");
const _ = require("lodash");

module.exports = {
    /**
     * Returns all tasks for a given user.
     */
    getAll: async function(req, res, next) {
        const userId = _.toNumber(req.params.userId);

        if (!_.isFinite(userId)) {
            res.status(400).error("Invalid user ID param");

            return next();
        }

        try {
            const tasks = await TaskService.listAllTasks(userId);

            res.status(200).json(tasks);
        } catch (err) {
            if (err instanceof UserErrors.UserNotFoundError) {
                res.status(404).error("User not found");
            } else {
                return next(err);
            }
        }
    },

    /**
     * Returns a single task for a given user.
     */
    get: async function(req, res, next) {
        const userId = _.toNumber(req.params.userId);
        const taskId = _.toNumber(req.params.taskId);

        if (!_.isFinite(userId)) {
            res.status(400).error("Invalid user ID param");

            return next();
        }

        if (!_.isFinite(taskId)) {
            res.status(400).error("Invalid task ID param");

            return next();
        }

        try {
            const task = await TaskService.getTaskInfo(taskId, userId);

            res.status(200).json(task);
        } catch (err) {
            if (err instanceof UserErrors.UserNotFoundError) {
                res.status(404).error("User not found");
            } else if (err instanceof TaskErrors.TaskNotFoundError) {
                res.status(404).error("Task not found");
            } else if (err instanceof TaskErrors.InvalidUserError) {
                res.status(403).error("User is not authorized to access task");
            } else {
                return next(err);
            }
        }
    },

    /**
     * Deletes a single task for a given user.
     */
    delete: async function(req, res, next) {
        const userId = _.toNumber(req.params.userId);
        const taskId = _.toNumber(req.params.taskId);

        if (!_.isFinite(userId)) {
            res.status(400).error("Invalid user ID param");

            return next();
        }

        if (!_.isFinite(taskId)) {
            res.status(400).error("Invalid task ID param");

            return next();
        }

        try {
            await TaskService.deleteTask(taskId, userId);

            res.status(204).json();
        } catch (err) {
            if (err instanceof UserErrors.UserNotFoundError) {
                res.status(404).error("User not found");
            } else if (err instanceof TaskErrors.TaskNotFoundError) {
                res.status(404).error("Task not found");
            } else if (err instanceof TaskErrors.InvalidUserError) {
                res.status(403).error("User is not authorized to access task");
            } else {
                return next(err);
            }
        }
    },

    /**
     * Creates a new task for a user.
     */
    create: async function(req, res, next) {
        const userId = _.toNumber(req.params.userId);

        if (!_.isFinite(userId)) {
            res.status(400).error("Invalid user ID param");

            return next();
        }

        const payload = Joi.validate(req.body, Joi.object().keys({
            "name": Joi.string().required(),
            "description": Joi.string().required(),
            "date_time": Joi.date().iso().required(),
        }), {
            stripUnknown: {
                objects: true,
            },
            abortEarly: false,
        });

        if (payload.error) {
            res.status(400).error("Request failed validation", payload.error);
        } else {
            try {
                const task = await TaskService.createTask(userId, payload.value);

                res.status(201).json(task);
            } catch (err) {
                if (err instanceof UserErrors.UserNotFoundError) {
                    res.status(404).error("User not found");
                } else {
                    return next(err);
                }
            }
        }
    },

    /**
     * Updates a task for the given user.
     */
    update: async function(req, res, next) {
        const userId = _.toNumber(req.params.userId);
        const taskId = _.toNumber(req.params.taskId);

        if (!_.isFinite(userId)) {
            res.status(400).error("Invalid user ID param");

            return next();
        }

        if (!_.isFinite(taskId)) {
            res.status(400).error("Invalid task ID param");

            return next();
        }

        const payload = Joi.validate(req.body, Joi.object().keys({
            "name": Joi.string(),
            "description": Joi.string(),
            "date_time": Joi.date().iso(),
        }), {
            stripUnknown: {
                objects: true,
            },
            abortEarly: false,
        });

        if (payload.error) {
            res.status(400).error("Request failed validation", payload.error);
        } else {
            try {
                const task = await TaskService.updateTask(taskId, userId, payload.value);

                res.status(200).json(task);
            } catch (err) {
                if (err instanceof UserErrors.UserNotFoundError) {
                    res.status(404).error("User not found");
                } else if (err instanceof TaskErrors.TaskNotFoundError) {
                    res.status(404).error("Task not found");
                } else if (err instanceof TaskErrors.InvalidUserError) {
                    res.status(403).error("User is not authorized to access task");
                } else {
                    return next(err);
                }
            }
        }
    },
};
