/**
 * General middleware functions.
 *
 * @module lib/http/middleware
 */

"use strict";

const logger = require("../logger");
const _ = require("lodash");

module.exports = {
    /**
     * Manages unhandled errors/exceptions.
     */
    error: function(err, req, res, next) {
        if (res.headersSent) {
            return next(err);
        }

        const status = err.status || 500;

        res.status(500).json("An unknown error occured");

        if (process.env.APP_ENV === "DEVELOP" || status >= 500) {
            logger.error(err);
        }
    },

    /**
     * Adds helpful helpers (i.e., error formatting) to the response object.
     */
    response: function(req, res, next) {
        res.error = (message, validationError) => {
            const payload = {
                message,
            };

            if (validationError && validationError.isJoi) {
                payload.errors = _.map(validationError.details, (error) => {
                    return {
                        field: error.path,
                        message: error.message,
                    };
                });
            }

            res.json(payload);
        };

        return next();
    },
};
