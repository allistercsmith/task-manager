/**
 * General user-related errors.
 *
 * @module lib/users/errors
 */

"use strict";

class UserNotFoundError extends Error {
    constructor(...args) {
        super(...args);

        Error.captureStackTrace(this, UserNotFoundError);
    }
}

class UsernameExistsError extends Error {
    constructor(...args) {
        super(...args);

        Error.captureStackTrace(this, UsernameExistsError);
    }
}

module.exports = {
    UserNotFoundError,
    UsernameExistsError,
};
