/**
 * User domain model.
 *
 * @module lib/users/user
 */

"use strict";

const db = require("../db");
const _ = require("lodash");

const User = db.Model.extend({
    tableName: "users",
}, {
    /**
     * Finds a user by username.
     *
     * @param {string} username The username to search by
     * @return {Promise}
     */
    findByUsername: async function(username) {
        if (!_.isString(username)) {
            throw new Error("`username` needs to be a string");
        }

        return this.where("username", username).fetch();
    },

    /**
     * Finds a user by ID.
     *
     * @param {number} id The user ID to search by
     * @return {Promise}
     */
    findById: async function(id) {
        if (!_.isFinite(id)) {
            throw new Error("`id` needs to be a number");
        }

        return this.where("id", id).fetch();
    },

    /**
     * Finds all users.
     *
     * @return {Promise}
     */
    findAll: async function() {
        return this.fetchAll();
    },
});

module.exports = User;
