/**
 * User use cases.
 *
 * @module lib/users/service
 */

"use strict";

const errors = require("./errors");
const User = require("./user");
const _ = require("lodash");

module.exports = {
    /**
     * Creates a new user.
     *
     * Will throw a UsernameExistsError if a user with the same
     * username already exists.
     *
     * @param {object} data The data to build the new user from
     * @return {object}
     */
    createUser: async function(data) {
        if (!_.isPlainObject(data)) {
            throw new Error("`data` needs to be a plain object");
        }

        let user = await User.findByUsername(data["username"]);

        if (!_.isNil(user)) {
            throw new errors.UsernameExistsError();
        }

        user = await (new User(data)).save();

        return user.toJSON();
    },

    /**
     * Updates an existing user, and returns the updated user.
     *
     * Will throw the following exceptions:
     *
     * 1) Will throw a UserNotFoundError if the user cannot be found.
     * 2) Will throw a UsernameExistsError if trying to update the username
     *    to one that already exists.
     *
     * @param {number} id The user's ID
     * @param {object} data The data to update the user with
     * @return {object}
     */
    updateUser: async function(id, data) {
        if (!_.isNumber(id)) {
            throw new Error("`id` needs to be a number");
        }

        if (!_.isPlainObject(data)) {
            throw new Error("`data` needs to be a plain object");
        }

        if (data["username"]) {
            const user = await User.findByUsername(data["username"]);

            if (!_.isNil(user) && user["id"] !== id) {
                throw new errors.UsernameExistsError();
            }
        }

        let user = await User.findById(id);

        if (_.isNil(user)) {
            throw new errors.UserNotFoundError();
        }

        user.set(data);

        user = await user.save();

        return user.toJSON();
    },

    /**
     * Deletes an existing user.
     *
     * Will throw a UserNotFoundError if the user cannot be found.
     *
     * @param {number} id The user's ID
     * @return {object}
     */
    deleteUser: async function(id) {
        if (!_.isNumber(id)) {
            throw new Error("`id` needs to be a number");
        }

        const user = await User.findById(id);

        if (_.isNil(user)) {
            throw new errors.UserNotFoundError();
        }

        await user.destroy();
    },

    /**
     * Returns a list of all users.
     *
     * @return {object}
     */
    listAllUsers: async function() {
        const users = await User.findAll();

        return users.toJSON();
    },

    /**
     * Returns a single user's profile.
     *
     * Will throw a UserNotFoundError if the user cannot be found.
     *
     * @param {number} id The user's ID
     * @return {object}
     */
    getUserInfo: async function(id) {
        if (!_.isNumber(id)) {
            throw new Error("`id` needs to be a number");
        }

        const user = await User.findById(id);

        if (_.isNil(user)) {
            throw new errors.UserNotFoundError();
        }

        return user.toJSON();
    },
};
