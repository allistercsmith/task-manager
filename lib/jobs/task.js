/**
 * Task-related jobs.
 *
 * @module lib/jobs/task
 */

"use strict";

const Task = require("../tasks/service");

module.exports = {
    /**
     * Marks overdue tasks (where their date_time is in the past)
     * as done.
     *
     * @return {Promise}
     */
    processOverdueTasks: async function() {
        return Task.updateOverdueTasks();
    },
};
